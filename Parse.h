/*
 * Parse.h
 *
 *  Created on: May 28, 2010
 *      Author: pchanda
 */

#ifndef PARSE_H_
#define PARSE_H_

#include "Common.h"

class Parse {
public:
	Parse();
	virtual ~Parse();
        string dbsnppath;
        string hapmappath;
        string srcpath;
        int chr;
        int comp;
        string destination;
        void parse(int argc, char* argv[]);
        int version; 
};

#endif /* PARSE_H_ */
