#include "Parse.h"

Parse::Parse()
{
   dbsnppath = "";
   hapmappath = "";
   destination = "";
   srcpath = "";
   version = -1;
   chr = 0;
   comp = 0;
}

Parse::~Parse()
{
}

void Parse::parse(int argc, char* argv[])
{
   for(int i=1;i<argc;i+=2)
   {
      stringstream ss;
      ss<<argv[i];
      string s = ss.str();
      if(s.compare("--dbsnp")==0)
      {
         if(i+1<argc)
            dbsnppath = argv[i+1]; 
         else
         {
            cout<<"dbsnp path missing !!!\n";
            exit(0);
         }
      } 
      if(s.compare("--hapmap")==0)
      {
         if(i+1<argc)
            hapmappath = argv[i+1]; 
         else
         {
            cout<<"hapmap path missing !!!\n";
            exit(0);
         }
      } 
      if(s.compare("--chr")==0)
      {
         if(i+1<argc)
            chr = atoi(argv[i+1]); 
         else
         {
            cout<<"chr missing !!!\n";
            exit(0);
         }
      } 
      if(s.compare("--c")==0)
      {
         if(i+1<argc)
            comp = atoi(argv[i+1]); 
         else
         {
            cout<<"compression flag missing !!!\n";
            exit(0);
         }
      } 
      if(s.compare("--out")==0)
      {
         if(i+1<argc)
            destination = argv[i+1]; 
         else
         {
            cout<<"destination missing !!!\n";
            exit(0);
         }
      } 
      if(s.compare("--src")==0)
      {
         if(i+1<argc)
            srcpath = argv[i+1]; 
         else
         {
            cout<<"src compressed file missing !!!\n";
            exit(0);
         }
      }
      if(s.compare("--ver")==0)
      {
         if(i+1<argc)
            version = atoi(argv[i+1]); 
         else
         {
            cout<<"version number missing ! please enter one between 120 and 133\n";
            exit(0);
         }
      }
      if(s.compare("--help")==0)
      {
          cout<<"For Compression, run as : "<<endl;
          cout<<"./Main --c 1 --hapmap <hapmap path> --dbsnp <dbsnp path> --out <destination filename> --chr <chr> --ver <dbsnp version>"<<endl;
          cout<<"For Decompression, run as : "<<endl;
          cout<<"./Main --c 0 --src <compressed file name> --dbsnp <dbsnp path> --out <destination filename> --ver <dbsnp version>"<<endl;
          exit(0);
      } 
   }
   //if(DEBUG)
   {
      cout<<"Input options in effect....\n";
      cout<<"dbsnppath = ["<<dbsnppath<<"]"<<endl;
      cout<<"hapmappath = ["<<hapmappath<<"]"<<endl;
      cout<<"destination = ["<<destination<<"]"<<endl;
      cout<<"chr = ["<<chr<<"]"<<endl;
      cout<<"compression flag = ["<<comp<<"]"<<endl;
      cout<<"version = ["<<version<<"]"<<endl;
   }

   if(version<0)
   {
      cout<<"Please enter dbsnp version number (120 - 132) \n";
      exit(0);
   }
   if(version<120 || version>132)
   {
      cout<<"Warning : invalid version number entered, will be set to deafult = 132\n";
      version = 132;
   }
   
   if(comp==1)
   {
      //compression.
      if(hapmappath.compare("")==0)
      {
        cout<<"hapmap path missing !!!\n";
        exit(0);
      } 
      if(destination.compare("")==0)
      {
        cout<<"destination path missing !!!\n";
        exit(0);
      } 
      if(dbsnppath.compare("")==0)
      {
        cout<<"dbsnp path missing\n";
      } 
      if(chr==0)
      {
         cout<<"chr is missing !!!\n";
         exit(0);
      }
   }
   else
   {
      //decompression.
      if(srcpath.compare("")==0)
      {
        cout<<"source file is missing !!!\n";
        exit(0);
      } 
      if(destination.compare("")==0)
      {
        cout<<"destination path missing !!!\n";
        exit(0);
      } 
      if(dbsnppath.compare("")==0)
      {
        cout<<"dbsnp path missing\n";
      } 
      if(chr==0)
      {
         cout<<"chr is missing !!!\n";
         exit(0);
      }
   }
}

/*
int main(int argc, char* argv[])
{
   Parse P;
   P.parse(argc,argv);
}
*/
