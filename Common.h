/*
 * Common.h
 *
 *  Created on: Apr 18, 2010
 *      Author: pchanda
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <set>
#include <algorithm>
#include <vector>
#include <limits.h>
#include "output.h"
#include "input.h"
#include <sstream>
#include <iomanip>
#include <time.h>

using namespace std;

enum opType { SNP, DELETION, INSERTION };
enum TS { AT, AG, AC, TG, TC, GC}; //template snp positions.

//string dbsnpdir = "dbSNP/";

#endif /* COMMON_H_ */
