/*
 * Dbsnp.h
 *
 *  Created on: May 28, 2010
 *      Author: pchanda
 */

#ifndef DBSNP_H_
#define DBSNP_H_

#include "Common.h"

class Dbsnp {
public:
	Dbsnp();
	virtual ~Dbsnp();

	struct Entry
	{
	  int dbsnpPos;
	  string dbsnpName;
	  string strand;
	  string subToken;
	};
	typedef Entry dbsnpEntry;

	vector< dbsnpEntry >* DBSNP;
	int N;

	void read(const string &dbsnpFile );
	dbsnpEntry* get(int index);
};

#endif /* DBSNP_H_ */
