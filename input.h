#ifndef INPUT_H_
#define INPUT_H_

#include <vector>
#include <sstream>
#include "bitfile.h"
#include <stdlib.h>

using namespace std;

//read a vector of bits
void readBits( bit_file_c& bf, unsigned count, vector<bool>* bits );
//read an array of bits
void readBitArrays( bit_file_c& bf, bool* bits, unsigned cnt );
//read a vector of gene letters
void readBitGens( bit_file_c& bf, unsigned count, vector<char>* gens );
void readBitGens( bit_file_c& bf, string & );
//read a VINT 
unsigned readBitVINT( bit_file_c& bf );
//read a string
string readString( bit_file_c& bf );
//read BitMap
void readBitMap( bit_file_c& , vector<string>* , int& , int& );
//read DELs
void readDEL(bit_file_c& , vector<int>* , int );
//read INS
void readINS(bit_file_c& bf, vector<string>* INS, int ins_count);

#endif /*INPUT_H_*/
