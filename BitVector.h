/*
 * BitVector.h
 *
 *  Created on: May 21, 2010
 *      Author: pchanda
 */

#ifndef BITVECTOR_H_
#define BITVECTOR_H_

#include "Common.h"

class BitVector {
public:
	BitVector();
	BitVector(int, int);
	BitVector(string);
	void increment();
	void shiftRight();
	virtual ~BitVector();
	string getString();
        int len();

	vector<bool>* BV;
};

#endif /* BITVECTOR_H_ */
