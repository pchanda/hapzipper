
 /* Decompress.cpp
 *
 *  Created on: May 28, 2010
 *      Author: pchanda
 */

#include "Decompress_Hapmap.h"

Decompress_Hapmap::Decompress_Hapmap()
{
	BITMAP = new vector<bool>();
	nsBITMAP = new vector<bool>();
	BITVECTOR = new vector<bool>();

	NS_POS_TABLE = new vector<int>();
	NS_GEN_TABLE = new vector<string>();
	NS_BIT_TABLE = new vector< vector<string> >();
	NS_NAME_TABLE = new vector<string>();

	SF_POS_TABLE = new vector<int>();
	NM_POS_TABLE = new vector<int>();
	NM_NAME_TABLE = new vector<string>();
        HMflag = false; //hapmap absent for comparison.
}

Decompress_Hapmap::~Decompress_Hapmap()
{
	// TODO Auto-generated destructor stub
}

void Decompress_Hapmap::clear()
{
	BITMAP->clear();
	nsBITMAP->clear();
	BITVECTOR->clear();
	NS_POS_TABLE->clear();
	NS_GEN_TABLE->clear();
	NS_BIT_TABLE->clear();
	NS_NAME_TABLE->clear();
	SF_POS_TABLE->clear();
	NM_POS_TABLE->clear();
	NM_NAME_TABLE->clear();
}

void Decompress_Hapmap::readDBSNP(string path)
{
        cout<<"Reading "<<path.c_str()<<" for decompressing..."<<endl;
        DB.read(path.c_str());
}

//read the header of any chromosome in compressed form.
void Decompress_Hapmap::read_header(bit_file_c & srcBf)
{
	stringstream ss;
	string x = readString(srcBf);
	while(x.compare("eoh")!=0)
	{
		ss<<x<<" ";
		x = readString(srcBf);
	}
	HEADER = ss.str();
	//cout<<"Read header = "<<HEADER<<endl;
}

void Decompress_Hapmap::read_and_join(string src_bitmap1,string src_bitmap2,string src_bitvector,string outfile)
{
    FILE * in_1 = fopen( src_bitmap1.c_str() , "r");
    FILE * in_2 = fopen( src_bitmap2.c_str() , "r");
    FILE * in = fopen( src_bitvector.c_str() , "r");
    FILE * out = fopen( outfile.c_str() , "w");
    char c=0;
    //fscanf(in,"%d",&c);
    c = fgetc(in);
    int i = 0;
    while(c!=EOF)
    {
       if(c=='0')
       {
          //printf("%d c = %c\n",i,c);
          char buff[1000];
          //printf("Writing from %s\n",src_bitmap1.c_str()); 
          fgets( buff, 1000, in_1 );
          fputs(buff,out);
          i++;
       }
       else if(c=='1')
       { 
          //printf("%d c = %c\n",i,c);
          char buff[1000];
          //printf("Writing from %s\n",src_bitmap2.c_str()); 
          fgets( buff, 1000, in_2 );
          fputs(buff,out);
          i++;
       }
       //printf("===============\n");
       //fscanf(in,"%d",&c);
       c = fgetc(in);
    }

    fclose(in_1);
    fclose(in_2);
    fclose(in);
    fclose(out);
}

void Decompress_Hapmap::read_1(string src_bitmap,string src_bitvector,string src_nsbitmap)
{
    FILE * in_stream;
    in_stream = fopen( src_bitmap.c_str() , "r");
    int c = fgetc(in_stream);
    int n = 0; 
    while(c!=EOF)
    {
       if(!isspace(c))
       {
          if(c=='0')
             BITMAP->push_back(false);
          else 
             BITMAP->push_back(true);
          n++;
       }
       c = fgetc(in_stream);
    }
    cout<<"Decompress_Hapmap : "<<n<<" bitmap read\n";
    fclose(in_stream);
 
    in_stream = fopen( src_bitvector.c_str() , "r");
    c = fgetc(in_stream);
    n = 0; 
    while(c!=EOF)
    {
       if(!isspace(c))
       {
          if(c=='0')
             BITVECTOR->push_back(false);
          else 
             BITVECTOR->push_back(true);
          n++;
       }
       c = fgetc(in_stream);
    }
    cout<<"Decompress_Hapmap : "<<n<<" bitvector read\n";
    fclose(in_stream);

    in_stream = fopen( src_nsbitmap.c_str() , "r");
    c = fgetc(in_stream);
    n = 0; 
    while(c!=EOF)
    {
       if(!isspace(c))
       {
          if(c=='0')
             nsBITMAP->push_back(false);
          else 
             nsBITMAP->push_back(true);
          n++;
       }
       c = fgetc(in_stream);
    }
    cout<<"Decompress_Hapmap : "<<n<<" nsbitmap read\n";
    fclose(in_stream);
}


void Decompress_Hapmap::decompress(string src_bitmap,string src_bitvector,string src_others,string src_nsbitmap,string dbsnp_path,string outFile,string hapmap)
{
        read_1(src_bitmap,src_bitvector,src_nsbitmap);
	bit_file_c srcBf;
	srcBf.Open( src_others.c_str(), BF_READ );

	read_header(srcBf);

	int chr = readBitVINT( srcBf );
	//cout<<"Read chr "<<chr<<endl;
	CHR = chr;
	cout<<"\nProcessing chromosome = "<<chr<<endl;
	n_samples = readBitVINT( srcBf );
	string s = outFile.c_str();
	if(DBflag)
	{
	   readDBSNP(dbsnp_path);
	}
	read_new_snps(srcBf);
	if(DBflag)
	   read_others(srcBf);

	decompressSNPs(srcBf,s,hapmap);

	clear();
	srcBf.Close();
	//cout<<" ****************  END of Decompression **************** "<<endl;
}

void Decompress_Hapmap::process_new_snps(ofstream & destStream, int dbsnpPos, int & NS_ptr,ifstream & hapmapStream)
//void Decompress_Hapmap::process_new_snps(ofstream & destStream, int dbsnpPos, int & NS_ptr)
{
    if(NS_ptr>=NS_POS_TABLE->size())
       return;
    int ns_pos = (*NS_POS_TABLE)[NS_ptr];

    //cout<<"  Start :  in process new snp "<<ns_pos<<" < than dbsnp = "<<dbsnpPos<<endl;

    while(ns_pos<dbsnpPos)
    {
              //A new snp
              destStream<<(*NS_NAME_TABLE)[NS_ptr]<<" "<<(*NS_POS_TABLE)[NS_ptr]<<" ";
              //cout<<"    processing new snp "<<(*NS_NAME_TABLE)[NS_ptr]<<" "<<(*NS_POS_TABLE)[NS_ptr]<<endl;
              //next output the alleles using NS_GEN_TABLE and NS_BIT_TABLE.
              string geno = (*NS_GEN_TABLE)[NS_ptr];
              char A1 = geno[0];
              char A2 = geno[2];
              vector<string> V = (*NS_BIT_TABLE)[NS_ptr];
              vector<char> X;
              for(int t=0;t<n_samples;t++)
              {  
                 string allele = V[t];
                 if(allele.compare("0")==0)
                 {
                    destStream<<A1<<" ";
                    X.push_back(A1);
                 }
                 else
                 {
                    destStream<<A2<<" ";
                    X.push_back(A2);
                 }
              }
              destStream<<endl;
              if(HMflag)
                 read_and_compare(hapmapStream,(*NS_NAME_TABLE)[NS_ptr],(*NS_POS_TABLE)[NS_ptr],X);
              NS_ptr++;
              if(NS_ptr>=NS_POS_TABLE->size())
                 break;
              ns_pos = (*NS_POS_TABLE)[NS_ptr];
    }
    //cout<<"  Done :  in process new snp "<<ns_pos<<" < than dbsnp = "<<dbsnpPos<<endl;
}
 
string Decompress_Hapmap::flip_strand( string subToken)
{
        //reverse strand in dbSNP.
        char t0 = 'X';
        char t2 = 'X';
        switch(subToken[0])
        {
          case 'A': t0 = 'T';break;
          case 'G': t0 = 'C';break;
          case 'C': t0 = 'G';break;
          case 'T': t0 = 'A';break;
          default : cout<<"No match : "<<subToken[0];
        }
        switch(subToken[2])
        {
          case 'A': t2 = 'T';break;
          case 'G': t2 = 'C';break;
          case 'C': t2 = 'G';break;
          case 'T': t2 = 'A';break;
          default : cout<<"No match : "<<subToken[2];
        }
        stringstream ss;
        ss<<t0<<"/"<<t2;
        return ss.str();
}

//matches with original hapmap file line by line, if mismatch throws out error.
void Decompress_Hapmap::read_and_compare(ifstream & hapmap_org,string snp_name,int pos,vector<char> alleles)
{
       //return;

        string hapmapline = "";
        getline( hapmap_org, hapmapline );
        string hapmapname="";
        int hapmappos=INT_MAX;
        //Tokenize the line 
        if(hapmapline.compare("")!=0) //hapmapfile has not ended.
        {    
                hapmapname = strtok ( (char *)hapmapline.c_str(), "\t, " ); //type
                hapmappos = atoi( strtok(NULL, "\t, ") );  //pos
                
                if(hapmapname.compare(snp_name)!=0)
                {
                    cout<<"Error in matching at : "<<snp_name<<" , "<<pos<<" and "<<hapmapname<<" , "<<hapmappos<<endl;
                    exit(0);
                }

                char* x = strtok ( NULL, "\t, " ); 
                int i = 0;
                while(x!=NULL)
                {
                   char allele = x[0]; 
                   if(x[0]=='A'||x[0]=='G'||x[0]=='T'||x[0]=='C')
                   {
                    if(allele!=alleles[i])
                    {
                        cout<<"Error in allele matching for : "<<snp_name<<" , "<<pos<<" at position = "<<i<<endl;
                        cout<<allele<<" != "<<alleles[i]<<endl;
                        exit(0);
                    }
                    i++; 
                   }  
                   x = strtok ( NULL, "\t, " );
                }
        }
        //cout<<"  Matched  "<<snp_name<<" "<<pos<<endl;   
}

void Decompress_Hapmap::decompressSNPs(bit_file_c & srcBf, string & destFile, string & hapmapFile)
//void Decompress_Hapmap::decompressSNPs(bit_file_c & srcBf, string & destFile)
{
	string hapmapline = "";
	ifstream hapmapStream;
        if(hapmapFile.compare("")!=0)
        {
           HMflag = true;
	   hapmapStream.open( hapmapFile.c_str(), ifstream::in);
	   getline( hapmapStream, hapmapline );//skip header.
        }

    //cout<<"\n*********************************************\n\nIn decompressSNPs..........................with "<<destFile<<endl;
    ofstream destStream( destFile.c_str() );
    int dbsnp_index = 0;
    int N = BITVECTOR->size();
    int NS_ptr = 0;
    int SF_ptr = 0;
    int NM_ptr = 0;
    int event_ptr = 0;
    int bitmap_index = 0; //index for bitmap 
    int bitvector_index = 0; //index for bitvector

    destStream<<HEADER<<endl;

    //++++ change for no dbsnp starts
    if(DBflag==false)
    {
    	//cout<<"DBSNP absent"<<endl;
        cout<<"Processing all snps as new snps\n";
    	//process_new_snps(destStream,INT_MAX,NS_ptr);//output all new snps whose positions are < dbsnpPos.
    	   process_new_snps(destStream,INT_MAX,NS_ptr,hapmapStream);//output all new snps whose positions are < dbsnpPos.
		destStream.close();
             if(HMflag)
		hapmapStream.close();
		cout<<"Done de-compression to "<<destFile<<endl;
		return;

    }
    //---- change for no dbsnp ends.

    while (bitvector_index < N)
    {
        bool bit = (*BITVECTOR)[bitvector_index];
        Dbsnp::dbsnpEntry* DE = DB.get(dbsnp_index); //get a line from dbsnp.
        //process_new_snps(destStream,DE->dbsnpPos,NS_ptr);//output all new snps whose positions are < dbsnpPos.
        process_new_snps(destStream,DE->dbsnpPos,NS_ptr,hapmapStream);//output all new snps whose positions are < dbsnpPos.
        if(bit==false)
        {
           //A case pf snp deletion.
           //So nothing to output.
           //cout<<"                               Event : deletion "<<DE->dbsnpPos<<"  "<<DE->dbsnpName<<endl;
           bitvector_index++;
        }
        else
        {
           //Match in position.
           //Can be : 1. Good snp 2. New snp 3. SF snp 4.NM snp.
           int ns_pos = -1;
           int sf_pos = -1;
           int nm_pos = -1;
          
           if(NS_ptr<NS_POS_TABLE->size())
              ns_pos = (*NS_POS_TABLE)[NS_ptr];
           if(SF_ptr<SF_POS_TABLE->size())
              sf_pos = (*SF_POS_TABLE)[SF_ptr];
           if(NM_ptr<NM_POS_TABLE->size())
              nm_pos = (*NM_POS_TABLE)[NM_ptr];

           if(ns_pos==DE->dbsnpPos)
           {
              //A new snp but match in position with dbsnp.
              //next output the alleles using NS_GEN_TABLE and NS_BIT_TABLE.
              string snp_name = (*NS_NAME_TABLE)[NS_ptr];
              //cout<<"Event : new snp but match in position with dbsnp : "<<snp_name<<endl;
              destStream<<snp_name<<" "<<DE->dbsnpPos<<" ";
              string geno = (*NS_GEN_TABLE)[NS_ptr];
              char A1 = geno[0];
              char A2 = geno[2];
              //cout<<"Geno = "<<geno<<endl;
              vector<string> V = (*NS_BIT_TABLE)[NS_ptr];
              vector<char> X;
              for(int t=0;t<n_samples;t++)
              {  
                 string allele = V[t];
                 if(allele.compare("0")==0)
                 {
                    destStream<<A1<<" ";
                    X.push_back(A1);
                 }
                 else
                 {
                    destStream<<A2<<" ";
                    X.push_back(A2);
                 }
              }
              destStream<<endl;
              bitvector_index++;
              NS_ptr++;

              if(HMflag)
                read_and_compare(hapmapStream,snp_name,DE->dbsnpPos,X);
           }
           else if(sf_pos==event_ptr)
           {
              //A strand flipped snp
              string snp_name = DE->dbsnpName;
              //cout<<"Event : strand flipped snp : "<<snp_name<<endl;
              if((*NM_POS_TABLE)[NM_ptr]==event_ptr)
              {
                 snp_name = (*NM_NAME_TABLE)[NM_ptr];
                 NM_ptr++;
              }
              destStream<<snp_name<<" "<<DE->dbsnpPos<<" ";
              //next output the alleles after flipping dbsnp alleles, use next n_samples bits from BITMAP.
              string geno = flip_strand(DE->subToken);
              char A1 = geno[0];
              char A2 = geno[2];
              //bitmap_index = bitmap_index + 1;
              vector<char> X;
              for(int t=0;t<n_samples;t++)
              {
                 if((*BITMAP)[bitmap_index++]==false)
                 {
                    destStream<<A1<<" ";
                    X.push_back(A1);
                 }
                 else
                 {
                    destStream<<A2<<" ";
                    X.push_back(A2);
                 }
              }
              destStream<<endl;
              SF_ptr++;
              bitvector_index++;

              if(HMflag)
                read_and_compare(hapmapStream,snp_name,DE->dbsnpPos,X);
           }
           else if(nm_pos==event_ptr)
           {
              //A name mismatch snp
              string snp_name = (*NM_NAME_TABLE)[NM_ptr];
              //cout<<"Event : name mismatch snp : "<<snp_name<<endl;
              destStream<<snp_name<<" "<<DE->dbsnpPos<<" ";
              //next output the alleles using dbsnp alleles, use next n_samples bits from BITMAP.
              string geno = DE->subToken;
              char A1 = geno[0];
              char A2 = geno[2];
              //bitmap_index = bitmap_index + 1;
              vector<char> X;
              for(int t=0;t<n_samples;t++)
              {
                 if((*BITMAP)[bitmap_index++]==false)
                 {
                    destStream<<A1<<" ";
                    X.push_back(A1);
                 }
                 else
                 {
                    destStream<<A2<<" ";
                    X.push_back(A2);
                 }
              }
              destStream<<endl;
              NM_ptr++;
              bitvector_index++;

              if(HMflag)
                read_and_compare(hapmapStream,snp_name,DE->dbsnpPos,X);
           }
           else
           {
              //A good snp
              string snp_name = DE->dbsnpName;
              //cout<<"Event : good snp  : "<<snp_name<<endl;
              destStream<<snp_name<<" "<<DE->dbsnpPos<<" ";
              //next output the alleles using dbsnp alleles, use next n_samples bits from BITMAP.
              string geno = DE->subToken;
              char A1 = geno[0];
              char A2 = geno[2];
              //bitmap_index = bitmap_index + 1;
              vector<char> X;
              for(int t=0;t<n_samples;t++)
              {
                 if((*BITMAP)[bitmap_index++]==false)
                 {
                    X.push_back(A1);
                    destStream<<A1<<" ";
                    //cout<<A1;
                 }
                 else
                 {
                    X.push_back(A2);
                    destStream<<A2<<" ";
                 }
              }
              destStream<<endl;
              bitvector_index++;
              //cout<<endl;
              if(HMflag)
                read_and_compare(hapmapStream,snp_name,DE->dbsnpPos,X);
           }
        }
        dbsnp_index++;
        event_ptr++;
    }
    destStream.close();
    if(HMflag) 
      hapmapStream.close();
    cout<<"Done de-compression to "<<destFile<<endl;
}

void Decompress_Hapmap::read_new_snps(bit_file_c & srcBf)
{
   int cnt_new_snp = readBitVINT(srcBf);
   cout<<cnt_new_snp<<" new snps present"<<endl;
   //read position table. 
   int pos = 0;
   for(int i=0;i<cnt_new_snp;i++)
   {
      int delta = readBitVINT(srcBf);
      pos = pos + delta;
      NS_POS_TABLE->push_back(pos);
   }
   cout<<cnt_new_snp<<" new snp positions read "<<endl;
   //read genotype table. 
   for(int i=0;i<cnt_new_snp;i++)
   {
      string geno = "";
      readBitGens(srcBf,geno);
      NS_GEN_TABLE->push_back(geno);
   }
   cout<<cnt_new_snp<<" new snp genotypes read "<<endl;

   //read new snp names.
   cout<<"Reading new snp names"<<endl; 
   for(int i=0;i<cnt_new_snp;i++)
   {
      string name = readString(srcBf);
      NS_NAME_TABLE->push_back(name);
   }
   cout<<cnt_new_snp<<" new snp names read "<<endl;

   //populate NS_BIT_TABLE with nsBITMAP
   int n = nsBITMAP->size();
   int i = 0;
   while(i<n) 
   {
      vector<string> V;
      for(int j=0;j<n_samples;j++)
      {
         if((*nsBITMAP)[i]==false)
            V.push_back("0");
         else
            V.push_back("1");
         i++; 
      } 
      NS_BIT_TABLE->push_back(V);
   }
   //cout<<"Created NS_BIT_TABLE successfully..."<<endl;
}

void Decompress_Hapmap::read_others(bit_file_c & srcBf)
{
   int cnt_sf_snp = readBitVINT(srcBf);
   //cout<<cnt_sf_snp<<" strand flipped snps present"<<endl;
   //read position table. 
   int pos = 0;
   for(int i=0;i<cnt_sf_snp;i++)
   {
      int delta = readBitVINT(srcBf);
      pos = pos + delta;
      SF_POS_TABLE->push_back(pos);
   }
   cout<<cnt_sf_snp<<" strand flipped positions read "<<endl;
   int cnt_nm_snp = readBitVINT(srcBf);
   //cout<<cnt_nm_snp<<" name mismatch snps present"<<endl;
   //read position table. 
   pos = 0;
   for(int i=0;i<cnt_nm_snp;i++)
   {
      int delta = readBitVINT(srcBf);
      pos = pos + delta;
      NM_POS_TABLE->push_back(pos);
   }
   //cout<<cnt_nm_snp<<" name mismatch positions read "<<endl;
   //read name mismatch snp names.
   cout<<"Reading name mismatch snp names"<<endl; 
   for(int i=0;i<cnt_nm_snp;i++)
   {
      string name = readString(srcBf);
      NM_NAME_TABLE->push_back(name);
   }
   cout<<cnt_nm_snp<<" name mismatch names read "<<endl;
}
