/*
 * Compress_Dbsnp.c
 *
 *  Created on: May 28, 2010
 *      Author: pchanda
 */

/*
#include "Common.h"

int main()
{
     cout<<"Compressing dbsnp ..."<<endl;
     string destFile = "dbsnp.compressed"; 
     bit_file_c destBf;
     destBf.Open( destFile.c_str(), BF_WRITE );

     for(int chr=1;chr<23;chr++)
     {
        string dbsnppath = "..//dbsnp//";
        stringstream fname;
        fname<<dbsnppath<<"chr"<<chr<<".txt";
        string dbsnpFile = fname.str();
        cout<<"Processing chromosome "<<dbsnpFile<<endl;
        ifstream dbsnpStream;
        dbsnpStream.open( dbsnpFile.c_str() , ifstream::in);
	string dbsnpline = "";
	getline( dbsnpStream, dbsnpline ); //schema of dbsnp.
        int pos = 0;
	while(dbsnpline.compare("")!=0)
	{
		getline( dbsnpStream, dbsnpline ); //read a snp line from dbsnp.
		if(dbsnpline.compare("")!=0) //dbsnpfile has not ended.
		{
			strtok( (char *)dbsnpline.c_str(), "\t, " ); 	//ID
			strtok( NULL, "\t, " ); 	//chrNum
			int startPos = atoi( strtok( NULL, "\t, " ) ); 	//startPos
			int dbsnpPos = atoi( strtok(NULL, "\t, ") );  //endPos
			string dbsnpName = strtok( NULL, "\t, "); //name
			strtok( NULL, "\t, "); //score
			string strand = strtok( NULL, "\t, "); //strand
			strtok( NULL, "\t, "); //refNCBI
			strtok( NULL, "\t, "); //refUCSC
			string subToken = strtok( NULL, "\t, "); //observed
			if((dbsnpPos - startPos > 1) || ( dbsnpline.find("single") == string::npos ))
			{
				//not a snp, do not store
			}
			else
			{
                                //a snp of interest, write in compressed format.   
                                writeString(destBf,dbsnpName);
                                writeBitVINT(destBf,dbsnpPos-pos); 
                                writeString(destBf,strand);
                                writeString(destBf,subToken);
                                //cout<<"Written "<<dbsnpName<<" "<<(dbsnpPos-pos)<<" "<<strand<<" "<<subToken<<endl;
                                pos = dbsnpPos;
			}
		}
	}
        dbsnpStream.close();
     }
     cout<<"Done reading compressing dbsnp..."<<endl;
     destBf.ByteAlign();
     destBf.Close();
}
*/
