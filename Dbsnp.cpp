/*
 * Dbsnp.cpp
 *
 *  Created on: May 28, 2010
 *      Author: pchanda
 */

#include "Dbsnp.h"

Dbsnp::Dbsnp()
{
	// TODO Auto-generated constructor stub
	DBSNP = new vector< dbsnpEntry >();
	N = 0;
}

Dbsnp::~Dbsnp()
{
	// TODO Auto-generated destructor stub
	delete DBSNP;
}

void Dbsnp::read( const string &dbsnpFile )
{
	cout<<"Reading dbsnp into memory..."<<endl;
        DBSNP->clear();
    ifstream dbsnpStream;
    dbsnpStream.open( dbsnpFile.c_str() , ifstream::in);
	string dbsnpline = "";
	getline( dbsnpStream, dbsnpline ); //schema of dbsnp.

	while(dbsnpline.compare("")!=0)
	{
		getline( dbsnpStream, dbsnpline ); //read a snp line from dbsnp.
		dbsnpEntry E;
		if(dbsnpline.compare("")!=0) //dbsnpfile has not ended.
		{
			strtok( (char *)dbsnpline.c_str(), "\t, " ); 	//ID
			strtok( NULL, "\t, " ); 	//chrNum
			int startPos = atoi( strtok( NULL, "\t, " ) ); 	//startPos
			E.dbsnpPos = atoi( strtok(NULL, "\t, ") );  //endPos
			E.dbsnpName = strtok( NULL, "\t, "); //name
			strtok( NULL, "\t, "); //score
			E.strand = strtok( NULL, "\t, "); //strand
			strtok( NULL, "\t, "); //refNCBI
			strtok( NULL, "\t, "); //refUCSC
			E.subToken = strtok( NULL, "\t, "); //observed
			if((E.dbsnpPos - startPos > 1) || ( dbsnpline.find("single") == string::npos ))
			{
				//not a snp, do not store
			}
			else
			{
				DBSNP->push_back(E);
			}
		}
	}
	dbsnpStream.close();
	N = DBSNP->size();
	cout<<"Done reading dbsnp into memory...size is "<<N<<endl;
}


Dbsnp::dbsnpEntry* Dbsnp::get(int index)
{
	if(index<N)
		return &((*DBSNP)[index]);
	else
		return NULL;
}



