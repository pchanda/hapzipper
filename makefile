# $Id$	
# Date: 03/26/2010
# Author: Pritam Chanda

CC = g++
#CPPFLAGS = -g -ggdb  -O0
CPPFLAGS = -O3

all: Main

input.o: input.h 

output.o: output.h

Compress_Hapmap: Compress_Hapmap.h 

bitfile.o: bitfile.h

BitVector.o: BitVector.h

Decompress_Hapmap.o: Decompress_Hapmap.h

Dbsnp.o: Dbsnp.h

Parse.o: Parse.h

Main.o: input.h output.h   

Main : Main.o bitfile.o input.o output.o Compress_Hapmap.o BitVector.o Decompress_Hapmap.o Dbsnp.o Parse.o

clean:
	rm *.o
	rm Main 
