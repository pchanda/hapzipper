#!/bin/bash

#options
#------------------
#--hapmap <hapmap file path>
#--dbsnp  <dbsnp file path>
#--out    <file to write output>
#--chr    chromosome
#--ver    dbsnp version


#example with dbsnp
#---------------------
START=$(date +%s)
./Main --c 1 --hapmap ./hapmap/hapmap3_r2_b36_fwd.consensus.qc.poly.chr1_jpt+chb.unr.phased \
             --dbsnp ./dbsnp/chr1.txt \
             --out ./encoded/JPT.1 \
             --chr 1 --ver 132
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "HapZipper compression took $DIFF seconds"



#example without dbsnp
#---------------------

#START=$(date +%s)
#./Main --c 1 --hapmap ../hapmap/hapmap3_r2_b36_fwd.consensus.qc.poly.chr1_jpt+chb.unr.phased \
#             --out ./encoded/JPT.1 --chr 2 --ver 132
#END=$(date +%s)
#DIFF=$(( $END - $START ))
#echo "HapZipper compression took $DIFF seconds"
