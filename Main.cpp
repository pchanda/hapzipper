/*
 * Main.cpp
 *
 *  Created on: Mar 18, 2012
 *      Author: pchanda
 */


#include "Compress_Hapmap.h"
#include "Decompress_Hapmap.h"
#include "Parse.h"
#include <stdlib.h>
#include <sstream>

// check file existance
bool file_exists(const char *filename){
  FILE *file;
  if ((file = fopen(filename, "r"))) //I'm sure, you meant for READING =)
  {
     fclose(file);
     return true;
  }
  printf("File %s does not exist, quitting\n",filename);
  exit(1);
  //return false;
}

bool file_can_be_created(const char *filename){
  FILE *file;
  if ((file = fopen(filename, "w")))
  {
     remove(filename);
     return true;
  }
  printf("File %s cannot be created, quitting\n",filename);
  exit(1);
  //return false;
}

int main(int argc, char* argv[])
{ 
        Parse P;
        
        P.parse(argc,argv);

        string path = P.hapmappath;
        string src = P.srcpath;
        bool FLAG = P.comp;
        int chr = P.chr;
        string destination = P.destination;
        string dbsnp_path = P.dbsnppath;
	bool dbsnp_flag = true;
        char schr[20] = "";
        sprintf(schr,"%d",chr); 

        if(dbsnp_path.compare("")==0)
        {
           cout<<"Warning : no dbsnp path provided, proceeding assuming no dbsnp..."<<endl;
           dbsnp_flag = false;
        }
        else
        {
           file_exists(dbsnp_path.c_str());
        }

	if(FLAG)
	{
            //Testing
            /*
            string hapmap_path = "../../hapmap/JPT+CHB/hapmap3_r2_b36_fwd.consensus.qc.poly.chr1_jpt+chb.unr.phased";
            string destination_bitmap = "./encoded/JPT.bitmap.txt";
            string destination_bitvector = "./encoded/JPT.bitvector.txt";
            string destination_others = "./encoded/JPT.others.txt";
            string destination_nsbitmap = "./encoded/JPT.ns.bitmap.txt";
            */

            string outdir = destination.substr(0,destination.rfind("/"));
            string outfile = destination.substr(destination.rfind("/")+1,string::npos);
            printf("Output directory = %s \n",outdir.c_str());
            printf("Output filename = %s \n",outfile.c_str());

            string hapmap_path = path;
            file_exists(hapmap_path.c_str());

            file_can_be_created(destination.c_str());

            string destination_bitmap_1 = destination+".bitmap.1";
            string destination_bitmap_2 = destination+".bitmap.2";
            string destination_bitmap_vector = destination+".bitmap.vec";

            string destination_bitvector = destination+".bitvector";

            string destination_others = destination+".others";

            string destination_nsbitmap_1 = destination+".ns.bitmap.1";
            string destination_nsbitmap_2 = destination+".ns.bitmap.2";
            string destination_nsbitmap_vector = destination+".ns.bitmap.vec";

		Compress_Hapmap HC_1;
		bit_file_c destBf;
		HC_1.DBflag = dbsnp_flag;

                ofstream dest11( destination_bitmap_1.c_str() );
                ofstream dest12( destination_bitmap_2.c_str() );
                ofstream destvec1( destination_bitmap_vector.c_str() );

                ofstream dest2( destination_bitvector.c_str() );

                ofstream dest31( destination_nsbitmap_1.c_str() );
                ofstream dest32( destination_nsbitmap_2.c_str() );
                ofstream destvec3( destination_nsbitmap_vector.c_str() );

		destBf.Open( destination_others.c_str(), BF_WRITE );

		//@@@@@@@@@@@@@@@@@@  	COMPRESSION  STAGE   @@@@@@@@@@@@@@@@@@
 
		time_t t1,t2;

		HC_1.CHR = chr;
		cout<<"******* CHR "<<chr<<" *********"<<endl;

		if(dbsnp_flag)
			HC_1.readDBSNP(dbsnp_path);

		t1 = time(NULL);
		HC_1.compressSNPs(hapmap_path);
		t2 = time(NULL);
                cout<<"Generating bitmap took "<<difftime(t2, t1)<<" sec "<<endl;

		t1 = time(NULL);
	        HC_1.write_header(hapmap_path, destBf);
		HC_1.write_compressed(dest11,dest12,destvec1, dest2, dest31,dest32,destvec3, destBf);
		t2 = time(NULL);
                cout<<"Write took "<<difftime(t2, t1)<<" sec "<<endl;
		HC_1.clear();
                 
		writeBitVINT( destBf, 0);
		destBf.ByteAlign();
		destBf.Close();
                dest11.close();
                dest12.close();
                destvec1.close();
                dest2.close();
                dest31.close();
                dest32.close();
                destvec3.close();
   
                string dest_bitmap_1 = outfile+".bitmap.1";
                string dest_bitmap_2 = outfile+".bitmap.2";
                string dest_bitmap_vector = outfile+".bitmap.vec";
                string dest_bitvector = outfile+".bitvector";
                string dest_others = outfile+".others";
                string dest_nsbitmap_1 = outfile+".ns.bitmap.1";
                string dest_nsbitmap_2 = outfile+".ns.bitmap.2";
                string dest_nsbitmap_vector = outfile+".ns.bitmap.vec";

                t1 = time(NULL);
                cout<<"Bzip-ing and Tar-ing...\n";
                string command = "tar -cjf "+destination+".tar.bz2 "+" -C "+outdir+" "+dest_bitmap_1+" "+dest_bitmap_2+" "+dest_bitmap_vector+" "+dest_bitvector+" "+dest_others+" ";
                command += dest_nsbitmap_1+" "+dest_nsbitmap_2+" "+dest_nsbitmap_vector;
                int err = system(command.c_str());
                if(err>0)
                {
                   printf("Unexpected Error in tar-ing and bzip-ing = %d\n, quitting\n",err);
                   exit(1);
                }
                remove(destination_bitmap_1.c_str()); 
                remove(destination_bitmap_2.c_str()); 
                remove(destination_bitmap_vector.c_str()); 
                remove(destination_bitvector.c_str()); 
                remove(destination_others.c_str()); 
                remove(destination_nsbitmap_1.c_str()); 
                remove(destination_nsbitmap_2.c_str()); 
                remove(destination_nsbitmap_vector.c_str()); 
		t2 = time(NULL);
                cout<<"Others took "<<difftime(t2, t1)<<" sec "<<endl;

		HC_1.clear();
		cout<<"Done compression "<<endl;
		cout<<"--------------------"<<endl;
		cout<<"# New snps = "<<HC_1.count_new_snps<<endl;
		cout<<"# Sf snps = "<<HC_1.count_sf_snps<<endl;
		cout<<"# Nm snps = "<<HC_1.count_nm_snps<<endl;
		cout<<"# Total snps = "<<HC_1.count_total_snps<<endl;
	}
	else
	{
            /*
            string hapmap_path = "../../hapmap/JPT+CHB/hapmap3_r2_b36_fwd.consensus.qc.poly.chr1_jpt+chb.unr.phased";
            string src_bitmap = "./encoded/JPT.bitmap.txt";
            string src_bitvector = "./encoded/JPT.bitvector.txt";
            string src_others = "./encoded/JPT.others.txt";
            string src_nsbitmap = "./encoded/JPT.ns.bitmap.txt";
            string outfile = "./decoded/JPT.txt";
            */
            //printf("%s\n",src.c_str()); exit(0);

	    cout<<"Lets do decompression..."<<endl;
            cout<<"Untar-ing stuff...\n";
            file_exists(src.c_str());
            string outfile = destination;
            file_can_be_created(outfile.c_str());
            string command = "tar -xf "+src+" -C .";  
            int err = system(command.c_str());
            if(err>0)
            {
                printf("Unexpected Error in untar-ing = %d\n, quitting\n",err);
                exit(1);
            }
            string srcfile = src.substr(src.rfind("/")+1,string::npos);
            printf("Src file = %s\n",srcfile.c_str());
            string suffix = srcfile.substr(0,srcfile.rfind("tar"));
            printf("Suffix = %s\n",suffix.c_str());

            string hapmap_path = path;
            string src_bitmap1 = suffix+"bitmap.1";
            string src_bitmap2 = suffix+"bitmap.2";
            string src_vec = suffix+"bitmap.vec";

            string src_bitvector = suffix+"bitvector";
            string src_others = suffix+"others";

            string src_nsbitmap1 = suffix+"ns.bitmap.1";
            string src_nsbitmap2 = suffix+"ns.bitmap.2";
            string src_nsvec = suffix+"ns.bitmap.vec";

            //output after joining.
            string src_bitmap = suffix+"bitmap";
            string src_nsbitmap = suffix+"ns.bitmap";

	   //@@@@@@@@@@@@@@@@@@  	DE-COMPRESSION  STAGE   @@@@@@@@@@@@@@@@@@
	   Decompress_Hapmap DC;
	   DC.DBflag = dbsnp_flag;

           DC.read_and_join(src_bitmap1,src_bitmap2,src_vec,src_bitmap);
           DC.read_and_join(src_nsbitmap1,src_nsbitmap2,src_nsvec,src_nsbitmap);
           //cout<<"Now doing decompression............\n";fflush(stdout);
           time_t t1,t2;
           t1 = time(NULL);
	   DC.decompress(src_bitmap,src_bitvector,src_others,src_nsbitmap,dbsnp_path,outfile,hapmap_path);
           t2 = time(NULL);
           cout<<"Decompression took "<<difftime(t2, t1)<<" sec "<<endl;

           remove(src_bitmap1.c_str());
           remove(src_bitmap2.c_str());
           remove(src_vec.c_str());
           remove(src_bitvector.c_str());
           remove(src_others.c_str());
           remove(src_nsbitmap1.c_str());
           remove(src_nsbitmap2.c_str());
           remove(src_nsvec.c_str());
           remove(src_bitmap.c_str());
           remove(src_nsbitmap.c_str());
	}
}


