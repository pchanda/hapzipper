/*
 * Compress_Hapmap.cpp
 *
 *  Created on: Apr 30, 2010
 *      Author: pchanda
 */

/*
 *  Changes from V1.5
 *  DbSNP read into memory so that need no re-read for every chromosome.
 *  storing old copy of every line read is avoided by just using a function to read lines from dbsnp.
 */

#include "Compress_Hapmap.h"

Compress_Hapmap::Compress_Hapmap()
{
	NS_POS_TABLE = new vector<int>();
	NS_BP_TABLE = new vector<int>();
	NS_GEN_TABLE = new vector<string>();
	NS_NAME_TABLE = new vector<string>();
	NM_NAME_TABLE = new vector<string>();
	NM_POS_TABLE = new vector<int>();
	SF_POS_TABLE = new vector<int>();
}

Compress_Hapmap::~Compress_Hapmap()
{
	// TODO Auto-generated destructor stub
}

void Compress_Hapmap::readDBSNP(const string & dbsnpFile)
{
	DB.read(dbsnpFile);
}

void Compress_Hapmap::destroy() //destroy current tables.
{

}

void Compress_Hapmap::clear()
{
	BITMAP.clear();
	BITVECTOR.clear();
	NS_POS_TABLE->clear();
	NS_BP_TABLE->clear();
	NS_GEN_TABLE->clear();
	NS_BIT_TABLE.clear();
	NS_NAME_TABLE->clear();
	NM_NAME_TABLE->clear();
	NM_POS_TABLE->clear();
	SF_POS_TABLE->clear();
	DB.DBSNP->clear();
}

/*
void Compress_Hapmap::write_sorted_bitmap( )
{
        string x = "./encoded/OUT.txt";
        ofstream destStream( x.c_str() );
	cout<<"Writing BITMAP for chromosome = "<<CHR<<endl;
        //sort bitmap_new
        //sort (BITMAP_new.begin(), BITMAP_new.end(), my_sort);
        int n = BITMAP_new.size();
        for(int i=0;i<n;i++)
        {
           vector<bool> B = BITMAP_new[i];
           int m = B.size();
           for(int j=0;j<m;j++)
           {
              if(B[j])
                 destStream<<"1 ";
              else
                 destStream<<"0 ";
           }
           destStream<<"\n";
        }
        //write BITMAP_new
        destStream.close();

       //just the bitmap for match/mismatch 
        x = "./encoded/OUT_vector.txt";
        ofstream destStream1( x.c_str() );
	cout<<"Writing BITMAP match/mismatch for chromosome = "<<CHR<<endl;
        n = BITMAP_1.size();
        for(int i=0;i<n;i++)
        {
              if(BITMAP_1[i])
                 destStream1<<"1";
              else
                 destStream1<<"0";
        }
        destStream1<<"\n";
        destStream1.close(); 
}
*/

void Compress_Hapmap::write_compressed(ofstream & dest11, 
                                       ofstream & dest12,
                                       ofstream & destvec1,
                                       ofstream & dest2, 
                                       ofstream & dest31, 
                                       ofstream & dest32,
                                       ofstream & destvec3,
                                       bit_file_c & destBf)
{
	cout<<"Writing compressed data for chromosome = "<<CHR<<endl;

        //mean1.
        int i = 0;
        double mean1 = 0;
        for(i=0;i<SUM.size();i++)
           mean1 += SUM[i]; 
        mean1 /= SUM.size();

        //mean2.
        double mean2 = 0;
        for(i=0;i<NS_SUM.size();i++)
           mean2 += NS_SUM[i]; 
        mean2 /= NS_SUM.size();

	writeBitVINT( destBf, CHR);
	writeBitVINT( destBf, NSAMPLES);

        if(DBflag)
        {
 	    cout<<"Writing BITMAP for chromosome = "<<CHR<<endl;
            int n = BITMAP.size();
            for(i=0;i<n;i++)
            {
               vector<bool> B = BITMAP[i];
               int s = SUM[i];
               if(s<=mean1)
               {
                  int m = B.size();
                  for(int j=0;j<m;j++)
                  {
                     if(B[j]) dest11<<"1";
                     else dest11<<"0";
                  }  
                  dest11<<"\n";
                  destvec1<<"0\n";
               }
               else
               {
                  int m = B.size();
                  for(int j=0;j<m;j++)
                  {
                     if(B[j]) dest12<<"1";
                     else dest12<<"0";
                  }  
                  dest12<<"\n";
                  destvec1<<"1\n";
               }
            }

	    //////////////////////////////////////////////

	    cout<<"Writing BITMAP match/mismatch for chromosome = "<<CHR<<endl;
            n = BITVECTOR.size();
            for(i=0;i<n;i++)
            {
              if(BITVECTOR[i])
              {
                 //dest2<<i<<" 1\n";
                 dest2<<"1 ";
              }
              else
              {
                 //dest2<<i<<" 0\n";
                 dest2<<"0 ";
              }
            }
	    //////////////////////////////////////////////
        }

	//write New Snp Information.
	int n = NS_POS_TABLE->size();
	count_new_snps += n;
	writeBitVINT( destBf, n );//count of new snps.
	if(n>0)
	{
		int pos = (*NS_POS_TABLE)[0];
		writeBitVINT( destBf, pos );
		for(i=1;i<n;i++)
		{
			int new_pos = (*NS_POS_TABLE)[i];
			int offset = new_pos - pos;
			writeBitVINT( destBf, offset );
			pos = new_pos;
		}
		cout<<n<<" new snp positions written"<<endl;

		n = NS_GEN_TABLE->size();
		for(i=0;i<n;i++)
		{
			string geno = (*NS_GEN_TABLE)[i];
			//write a genotype.
			writeBitGens( destBf, geno );
		}
                cout<<n<<" new snp genotype alleles written"<<endl;

		n = NS_BIT_TABLE.size();
                for(i=0;i<n;i++)
                {
                   vector<bool> B = NS_BIT_TABLE[i];
                   int s = NS_SUM[i];
                   if(s<=mean2)
                   {
                      int m = B.size();
                      for(int j=0;j<m;j++)
                      {
                        if(B[j]) dest31<<"1";
                        else dest31<<"0";
                      }
                      dest31<<"\n";
                      destvec3<<"0\n";
                   }
                   else
                   {
                      int m = B.size();
                      for(int j=0;j<m;j++)
                      {
                        if(B[j]) dest32<<"1";
                        else dest32<<"0";
                      }
                      dest32<<"\n";
                      destvec3<<"1\n";
                   }
                }
                cout<<n<<" new snp genotype vectors written"<<endl;

		n = NS_NAME_TABLE->size();
		//no need to write n.
		cout<<n<<" new snp names written"<<endl;
		for(i=0;i<n;i++)
		{
			string snp_name = (*NS_NAME_TABLE)[i];
			writeString(destBf, snp_name);
		}
	}
	cout<<n<<" : New Snp information written"<<endl;

    //////////////////////////////////////////////

 if(DBflag)
 {

    //write strand flipped snp information.
    n = SF_POS_TABLE->size();
    count_sf_snps += n;
    writeBitVINT( destBf, n );
    cout<<n<<" Strand flipped Snp information written"<<endl;
    if(n>0)
    {
		int pos = (*SF_POS_TABLE)[0];
		writeBitVINT( destBf, pos );
		for(i=1;i<n;i++)
		{
			int new_pos = (*SF_POS_TABLE)[i];
			int offset = new_pos - pos;
			writeBitVINT( destBf, offset );
			pos = new_pos;
		}
    }

    //////////////////////////////////////////////

	//write Name mismatch Snp Information.
	n = NM_POS_TABLE->size();
	count_nm_snps += n;
	cout<<n<<" Name mismatch Snp information written"<<endl;
	writeBitVINT( destBf, n );
	if(n>0)
	{
		int pos = (*NM_POS_TABLE)[0];
		writeBitVINT( destBf, pos );
		for(i=1;i<n;i++)
		{
			int new_pos = (*NM_POS_TABLE)[i];
			int offset = new_pos - pos;
			writeBitVINT( destBf, offset );
			pos = new_pos;
		}
		n = NM_NAME_TABLE->size();
		//no need to write n.
		for(i=0;i<n;i++)
		{
			string snp_name = (*NM_NAME_TABLE)[i];
			writeString(destBf, snp_name);
		}
	}
    }
	cout<<"Done writing compressed data"<<endl;
	cout<<"# New snps = "<<count_new_snps<<endl;
	cout<<"# Sf snps = "<<count_sf_snps<<endl;
	cout<<"# Nm snps = "<<count_nm_snps<<endl;
	cout<<"# Total snps = "<<count_total_snps<<endl;
}

int Compress_Hapmap::encode(char allele, char *subToken)
{
	if(allele==subToken[0])
	{
		return 0;
	}
	else if(allele==subToken[2])
	{
		return 1;
	}
    else //mismatch.
    {
      	return -1;
    }
}

int Compress_Hapmap::encode_strand_flipped(char allele, char *subToken) //first flip the strand, then encode.
{
	char* subToken_flipped = flip_strand(subToken);
	if(allele==subToken_flipped[0])
	{
		return 0;
	}
	else if(allele==subToken_flipped[2])
	{
		return 1;
	}
	else //mismatch.
	{
		return -1;
	}
}

char* Compress_Hapmap::flip_strand(char* subToken)
{
	//reverse strand in dbSNP.
	char t0 = 'X';
	char t2 = 'X';
	switch(subToken[0])
	{
	  case 'A': t0 = 'T';break;
	  case 'G': t0 = 'C';break;
	  case 'C': t0 = 'G';break;
	  case 'T': t0 = 'A';break;
	  default : cout<<"No match : "<<subToken[0];
	}
	switch(subToken[2])
	{
	  case 'A': t2 = 'T';break;
	  case 'G': t2 = 'C';break;
	  case 'C': t2 = 'G';break;
	  case 'T': t2 = 'A';break;
	  default : cout<<"No match : "<<subToken[2];
	}
    char c = '/';
	char * arr = new char[3];
	arr[0] = t0;
	arr[1] = c;
	arr[2] = t2;
	return arr;
}

int Compress_Hapmap::encode_new_snp_alleles(vector<char> alleles,const char* subToken, vector<bool>& codes)
{
	int n = alleles.size();
        int s = 0;
	for(int i=0;i<n;i++)
	{
		if(alleles[i]==subToken[0])
			codes.push_back(false);
		else
                {
			codes.push_back(true);
                        s++;
                }
	}
        return s;
}

void Compress_Hapmap::readHAPMAP(ifstream & hapmapStream, Compress_Hapmap::hapmapEntry & HE)
{
	string hapmapline = "";
	getline( hapmapStream, hapmapline );
	HE.hapmapName="";
	HE.hapmapPos=INT_MAX;
	//Tokenize the line
	HE.allele1 = 'N';
	HE.allele2 = 'N';
	HE.alleles.clear();
	if(hapmapline.compare("")!=0) //hapmapfile has not ended.
	{
		HE.hapmapName = strtok ( (char *)hapmapline.c_str(), "\t, " ); //type
		HE.hapmapPos = atoi( strtok(NULL, "\t, ") );  //pos
		char* x = strtok ( NULL, "\t, " );
		HE.allele1 = x[0];
		while(x!=NULL)
		{
			if(x[0]=='A'||x[0]=='G'||x[0]=='T'||x[0]=='C')
			{
				HE.alleles.push_back(x[0]);
				if(x[0]!=HE.allele1)
				HE.allele2 = x[0];
			}
			x = strtok ( NULL, "\t, " );
		}
		count_total_snps++;
	}
	if(HE.alleles.size()>0)
		NSAMPLES = HE.alleles.size();
}

void Compress_Hapmap::compressSNPs(const string &hapmapFile)
{
		int dbsnp_index = 0;

	    ifstream hapmapStream;
	    hapmapStream.open( hapmapFile.c_str(), ifstream::in);

		string hapmapline = "";
		string hapmapline_old = "";

		cout << "In compress : with "<<hapmapFile<<endl;

		getline( hapmapStream, hapmapline ); //schema of hapmap.
                //cout<<"-------->"<<hapmapline<<endl;


		Compress_Hapmap::hapmapEntry HE;
		readHAPMAP(hapmapStream, HE);//read one line from the input hapmap.

		Dbsnp::dbsnpEntry* DE = NULL;

		if(DBflag)
			DE = DB.get(dbsnp_index); //get a line from dbsnp.

		//cout<<"dbsnp : "<<DE->dbsnpName<<" "<<DE->dbsnpPos<<" "<<DE->strand<<" "<<DE->subToken<<endl;
		//cout<<"HAPMAP : "<<HE.hapmapName<<" "<<HE.hapmapPos<<" "<<HE.allele1<<" "<<HE.allele2<<endl;

		bool run = true;
		int event_count = 0;
                int omitted_1 = 0;
                int omitted_2 = 0;

		do{
			//cout<<"dbsnp : "<<DE->dbsnpName<<" "<<DE->dbsnpPos<<" "<<DE->strand<<" "<<DE->subToken<<endl;
			//cout<<"HAPMAP : "<<HE.hapmapName<<" "<<HE.hapmapPos<<" "<<HE.allele1<<" "<<HE.allele2<<endl;
			//cout<<"-------------------------------------------------------------------------------------\n"<<endl;

			char* subToken;
			int dbsnpPos = INT_MAX;//INFINITY;
			string strand = "+";
			string dbsnpName = "";

			if(DE!=NULL)
			{
				dbsnpPos = DE->dbsnpPos;
				strand = DE->strand;
				dbsnpName = DE->dbsnpName;
				subToken = (char*)DE->subToken.c_str();
			}

			string hapmapName = "";
			int hapmapPos = INT_MAX;//INFINITY;

			if(HE.hapmapName.compare("")!=0)
			{
				hapmapName = HE.hapmapName;
				hapmapPos = HE.hapmapPos;
			}

			if( (dbsnpPos==hapmapPos) ) //match in position.
			{
                             //cout<<BITVECTOR.size()<<" 1  dbname = "<<dbsnpName<<"         hapmapname = "<<hapmapName<<"      pos = "<<dbsnpPos<<endl;

				if(dbsnpName.compare(hapmapName)==0) //match in snp name too.
				{
					int n = HE.alleles.size();
					vector<bool> codes;
                                        int sum = 0;
					BITVECTOR.push_back(true);//match in position.

					bool mismatch = false;
					for(int q=0;q<n;q++)
					{
						int code = encode(HE.alleles[q],subToken);
						if(code<0)
						{
							//some allele mismatch.
							mismatch = true;
							break;
						}
						if(code==0)
                                                {
							codes.push_back(false);
                                                }
						else
                                                {
                                                        sum++;
							codes.push_back(true);
                                                }
					}
					bool flipped = false;
					if(mismatch==true) //may be a strand issue, try flipping strands.
					{
						codes.clear();
                                                sum = 0;
						mismatch = false;
						for(int q=0;q<n;q++)
						{
							int code = encode_strand_flipped(HE.alleles[q],subToken);
							if(code<0)
							{
								//some allele mismatch.
								mismatch = true;
								break;
							}
							if(code==0)
                                                        {
								codes.push_back(false);
                                                        }
							else
                                                        {
                                                                sum++;
								codes.push_back(true);
                                                        }
						}
						if(mismatch==false)
								flipped = true;
					}
					if(mismatch==false)
					{ 
						//copy codes to BITMAP.
                                                BITMAP.push_back(codes);
                                                SUM.push_back(sum);

						if(codes.size()!=NSAMPLES){cout<<"Error 1 in sample size : "<<codes.size()<<endl;exit(0);}

						DE = DB.get(++dbsnp_index); //read a snp line from dbsnp.

						readHAPMAP(hapmapStream, HE);//read one line from the input hapmap.

						if(flipped==true)
							SF_POS_TABLE->push_back(event_count);

						event_count++;
					}
					else
					{
						//Has to store as a new snp.
						//cout<<"Warning 1 : alleles of snp "<<dbsnpName<<" at "<<dbsnpPos<<" do not match hapmap data, coded as new snp"<<endl;

						stringstream st;
						st<<HE.allele1<<"/"<<HE.allele2;

						NS_POS_TABLE->push_back(dbsnpPos);
						NS_GEN_TABLE->push_back(st.str());
						NS_NAME_TABLE->push_back(hapmapName);
						//store the alleles as a bit vector.
						vector<bool> ns_codes;
						int s = encode_new_snp_alleles(HE.alleles,(st.str()).c_str(), ns_codes);
						NS_BIT_TABLE.push_back(ns_codes);
                                                NS_SUM.push_back(s); 

						readHAPMAP(hapmapStream, HE);//read one line from the input hapmap.
						//advance dbsnp pointer too.
						if(DBflag)
							DE = DB.get(++dbsnp_index); //read a snp line from dbsnp.
				                event_count++;
					}
				}
				else //positions match, but names do not match, check if all alleles match, then you can encode.
				{
					//cout<<"Warning 2 : snp names "<<dbsnpName<<" and "<<hapmapName<<" do not match at "<<dbsnpPos<<endl;
					int n = HE.alleles.size();
					vector<bool> codes;
                                        int sum = 0;

					BITVECTOR.push_back(true);//match in position.

					bool mismatch = false;
					for(int q=0;q<n;q++)
					{
						int code = encode(HE.alleles[q],subToken);
						if(code<0)
						{
							//some allele mismatch.
							mismatch = true;
							break;
						}
						if(code==0)
                                                {
							codes.push_back(false);
                                                }
						else
                                                {
                                                        sum++;
							codes.push_back(true);
                                                }
					}
					bool flipped = false;
					if(mismatch==true) //may be a strand issue, try flipping strands.
					{
						 codes.clear();
                                                 sum = 0;
						 mismatch = false;
						 for(int q=0;q<n;q++)
						 {
							 int code = encode_strand_flipped(HE.alleles[q],subToken);
							 if(code<0)
							 {
								 //some allele mismatch.
								 mismatch = true;
								 break;
							 }
							if(code==0)
                                                        {
								codes.push_back(false);
                                                        }
							else
                                                        {
                                                                sum++;
								codes.push_back(true);
                                                        }
						 }
						 if(mismatch==false)
								 flipped = true;
					}
					if(mismatch==false)
					{
						//copy codes to BITMAP.
                                                BITMAP.push_back(codes);
                                                SUM.push_back(sum);

						if(codes.size()!=NSAMPLES){cout<<"Error 2 in sample size "<<codes.size()<<endl;exit(0);}
						DE = DB.get(++dbsnp_index); //read a snp line from dbsnp.
						readHAPMAP(hapmapStream, HE);//read one line from the input hapmap.
						NM_POS_TABLE->push_back(event_count);
						NM_NAME_TABLE->push_back(hapmapName);

						if(flipped==true)
							SF_POS_TABLE->push_back(event_count);

						event_count++;
					}
					else
					{
						//cout<<"Warning 2 : alleles of snp at "<<dbsnpPos<<" do not match hapmap data, coded as new snp"<<endl;
						//BITVECTOR.push_back(true);

						stringstream st;
						st<<HE.allele1<<"/"<<HE.allele2;

						NS_POS_TABLE->push_back(dbsnpPos);
						NS_GEN_TABLE->push_back(st.str());
						NS_NAME_TABLE->push_back(hapmapName);
						//store the alleles as a bit vector.
						vector<bool> ns_codes;
						int s = encode_new_snp_alleles(HE.alleles,(st.str()).c_str(),ns_codes);
						NS_BIT_TABLE.push_back(ns_codes);
                                                NS_SUM.push_back(s); 

						readHAPMAP(hapmapStream, HE);//read one line from the input hapmap.
						//advance dbsnp pointer too.
						if(DBflag)
							DE = DB.get(++dbsnp_index); //read a snp line from dbsnp.
				                event_count++;
					}
				}
			}
			else if(dbsnpPos > hapmapPos) //new snp.
			{
                                //No need to store bitmap.
				//cout<<"Warning 3 : alleles of snp at "<<hapmapPos<<" coded as new snp"<<endl;
				stringstream st;
				st<<HE.allele1<<"/"<<HE.allele2;
				NS_POS_TABLE->push_back(hapmapPos);
				NS_GEN_TABLE->push_back(st.str());
				NS_NAME_TABLE->push_back(hapmapName);
				//store the alleles as a bit vector.
				vector<bool> ns_codes;
				int s = encode_new_snp_alleles(HE.alleles,(st.str()).c_str(),ns_codes);
				NS_BIT_TABLE.push_back(ns_codes);
                                NS_SUM.push_back(s); 

				readHAPMAP(hapmapStream, HE);//read one line from the input hapmap.
			}
			else //deletion of a snp.
			{
                             //cout<<BITVECTOR.size()<<"    0  dbname = "<<dbsnpName<<"    "<<dbsnpPos<<endl;
				BITVECTOR.push_back(false);//mismatch
				if(DBflag)
					DE = DB.get(++dbsnp_index); //read a snp line from dbsnp.
				event_count++;
			}

			if(HE.hapmapName.compare("")==0 && DE==NULL)
				run = false;

			if(HE.hapmapName.compare("")==0 )
				run = false;

                        //cout<<BITMAP.size()<<" "<<dbsnp_index<<"\n";

		}while(run==true);
		cout<<"Done compressing snps...."<<event_count<<","<<dbsnp_index<<endl;
                cout<<BITMAP.size()<<" snp vectors encoded"<<endl;
                cout<<BITVECTOR.size()<<" bit positions for match/mismatch"<<endl;
}

//write the header of any chromosome in compressed form.
void Compress_Hapmap::write_header(string & hapmapFile, bit_file_c & destBf)
{
    ifstream hapmapStream;
    hapmapStream.open( hapmapFile.c_str(), ifstream::in);

	string hapmapline = "";
	getline( hapmapStream, hapmapline ); //schema of hapmap.
	string name = strtok ( (char *)hapmapline.c_str(), "\t, " ); //name
	writeString(destBf, name);
	string pos =  strtok(NULL, "\t, ");  //pos
	writeString(destBf, pos);
	char* x = strtok ( NULL, "\t, " );
	while(x!=NULL)
	{
                stringstream ss;
                ss<<x;
                string z = ss.str();
		writeString(destBf, z);
		x = strtok ( NULL, "\t, " );
	}
	string eoh = "eoh";
	writeString(destBf,eoh);
	hapmapStream.close();
}
