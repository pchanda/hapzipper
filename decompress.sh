#!/bin/bash

#options
#-------------------
#--dbsnp  <dbsnp file path>
#--src    <compressed file path>
#--out    <output decompressed file>
#--chr    chromosome
#--ver    version


#example with dbsnp
#---------------------
START=$(date +%s)
./Main --c 0 --dbsnp ./dbsnp/chr1.txt \
       --src ./encoded/JPT.1.tar.bz2 \
       --out ./decoded/JPT.1 --chr 1 --ver 132
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "HapZipper decompression took $DIFF seconds"



#example without dbsnp
#---------------------
#START=$(date +%s)
#./Main --c 0 --src ./encoded/JPT.1.tar.bz2 --out ./decoded/JPT.1 --chr 1 --ver 132
#END=$(date +%s)
#DIFF=$(( $END - $START ))
#echo "HapZipper decompression took $DIFF seconds"
