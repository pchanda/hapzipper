/*
 * Decompress.h
 *
 *  Created on: May 28, 2010
 *      Author: pchanda
 */

#ifndef DECOMPRESS_H_
#define DECOMPRESS_H_

//#include "KMerDecoding.h"
#include "Dbsnp.h"

class Decompress_Hapmap {
public:
    Dbsnp DB;

	vector<bool> *BITMAP;
	vector<bool> *nsBITMAP;
	vector<bool> *BITVECTOR;

	vector<int>* NS_POS_TABLE; //new snp position table.
	vector<string>* NS_GEN_TABLE; //new snp genotype table.
	vector< vector<string> >* NS_BIT_TABLE; //new snp bitmap table.
	vector<string>* NS_NAME_TABLE; //new snp names.

	vector<int>* NM_POS_TABLE; //name mismatch snp position table.
	vector<string>* NM_NAME_TABLE; //name mismatch snp names.

	vector<int>* SF_POS_TABLE; //strand flipped snp position table.

	//read from compressed file.
	int n_samples;
	int bitmap_length;
	int CHR;//current chromosome that is being decompressed.
	string HEADER;
	bool DBflag;

        bool HMflag;

	Decompress_Hapmap();
	virtual ~Decompress_Hapmap();

	void clear();
        void read_1(string src_bitmap,string src_bitvector,string src_nsbitmap);
	//void decompress( string src_bitmap,string src_bitvector,string src_others,string src_nsbitmap,string dbsnp_path,string outFile );
	//void decompressSNPs( bit_file_c &, string &);
	void read_new_snps( bit_file_c & );
	void read_others( bit_file_c & );
	void readDBSNP(string);
	//void process_new_snps(ofstream & destStream, int dbsnpPos, int & NS_ptr);
	string flip_strand(string s);
	void read_and_compare(ifstream & , string ,int ,vector<char> );
	void read_header(bit_file_c &);
        void read_and_join(std::string, std::string, std::string, std::string);

	void decompress( string src_bitmap,string src_bitvector,string src_others,string src_nsbitmap,string dbsnp_path,string outFile,string);
        void decompressSNPs(bit_file_c & srcBf, string & destFile, string & hapmapFile);
        void process_new_snps(std::ofstream&, int, int&, std::ifstream&);
};

#endif /* DECOMPRESS_H_ */
