/*
 * BitVector.cpp
 *
 *  Created on: May 21, 2010
 *      Author: pchanda
 */

#include "BitVector.h"

BitVector::BitVector()
{
	// TODO Auto-generated constructor stub
	BV = new vector<bool>();
}

BitVector::BitVector(int n, int v)
{
	BV = new vector<bool>();
	for(int i=0;i<n;i++)
	{
		if(v==1)
			BV->push_back(true);
		else if (v==0)
			BV->push_back(false);
		else
		{
			cout<<"Error : wrong code "<<v<<endl;
			exit(1);
		}
	}
}

BitVector::BitVector(string s)
{
	BV = new vector<bool>();
	const char* x = s.c_str();
	int n = s.size();
	for(int i=0;i<n;i++)
	{
		if(x[i]=='1')
			BV->push_back(true);
		else if (x[i]=='0')
			BV->push_back(false);
		else
		{
			cout<<"Error : wrong code "<<s<<endl;
			exit(1);
		}
	}
}

void BitVector::increment()
{
	int n = BV->size();
	bool set = false;
	for(int i=n-1;i>=0;i--)
	{
		if((*BV)[i]==false)
		{
			(*BV)[i] = true;
			set = true;
			break;
		}
		else
		{
			(*BV)[i] = false;
		}
	}
	if(set==false)
	{
		vector<bool>::iterator it;
		it = BV->begin();
		it = BV->insert ( it , true );
	}
}

void BitVector::shiftRight()
{
	BV->pop_back();
}

int BitVector::len()
{
	return BV->size();
}

string BitVector::getString()
{
	stringstream code;
	int n = BV->size();
	for(int i=0;i<n;i++)
	{
		if((*BV)[i]==true)
			code<<"1";
		else
			code<<"0";
	}
	//cout<<code.str()<<endl;
	return code.str();
}

BitVector::~BitVector()
{
	// TODO Auto-generated destructor stub
}

/*
int main()
{
	BitVector B(1,0);
	for(int i=0;i<20;i++)
	{
		cout<<B.getString()<<endl;
		B.increment();
	}
	cout<<B.getString()<<endl;
	B.shiftRight();
	cout<<B.getString()<<endl;
}
*/
