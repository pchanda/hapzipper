/*
 * Compress_Hapmap.h
 *
 *  Created on: Apr 30, 2010
 *      Author: pchanda
 */

#ifndef COMPRESS_HAPMAP_H_
#define COMPRESS_HAPMAP_H_

#include "Common.h"
//#include "KMerEncoding.h"
#include "Dbsnp.h"

class Compress_Hapmap {
public:
	Dbsnp DB;

	vector< vector<bool> > BITMAP; //only snp bitmap.
        vector<int> SUM;
	vector<bool>  BITVECTOR; //only match/mismatch in snp positions.

	vector<int>* NS_POS_TABLE; //new snp position table.
	vector<int>* NS_BP_TABLE; //new snp base pair position table.
	vector<string>* NS_GEN_TABLE; //new snp genotype table.
	vector< vector<bool> > NS_BIT_TABLE; //new snp bitmap table.
        vector<int> NS_SUM;
	vector<string>* NS_NAME_TABLE; //new snp names.

	vector<int>* NM_POS_TABLE; //name mismatch snp position table.
	vector<string>* NM_NAME_TABLE; //name mismatch snp names.

	vector<int>* SF_POS_TABLE; //strand flipped snp position table.

	int CHR;
	int NSAMPLES;

	bool DBflag;

	int count_new_snps;
	int count_sf_snps;
	int count_nm_snps;
	int count_total_snps;
	int count_samples;

public:

	struct Entry
	{
	  int hapmapPos;
	  string hapmapName;
	  vector<char> alleles;
	  char allele1;
	  char allele2;
	};
	typedef Entry hapmapEntry;


	Compress_Hapmap();
	void readDBSNP(const string &);
	void readHAPMAP(ifstream & hapmapStream, Compress_Hapmap::hapmapEntry & HE);
	virtual ~Compress_Hapmap();
	void compressSNPs(const string &);
	int encode(char, char *);
	string getLine(ifstream& );
	void destroy();
	void write();
	void write_compressed(ofstream &, ofstream &, ofstream &, ofstream &, ofstream &, ofstream &, ofstream &, bit_file_c & );
	void write_sorted_bitmap( );
	void print();
	char* flip_strand(char* );
	int encode_strand_flipped(char , char *); //first flip the strand, then encode.
	void clear();
	int encode_new_snp_alleles(vector<char> , const char* , vector< bool >& );
	void huffman_and_write(vector<bool>* BV, bit_file_c &,int Kmer,bool);
	void write_header(string &, bit_file_c &);
};

#endif /* COMPRESS_HAPMAP_H_ */
