#include "input.h"

//read a vector of bits
void readBits( bit_file_c& bf, unsigned count, vector<bool>* bits )
{
	  /* read single bits */
	  int value = 0;
	  for ( unsigned i = 0; i < count; i++ )
	  {
	      value = bf.GetBit();
	      if(value == EOF)
	      {
	         cerr << "Error: reading bit" << endl;
	         bf.Close();
	         return;
	      }
	      if(value == 1)
	      {
	    	  bits->push_back( true );	    	  
	      }
	      else
	      {
	    	  bits->push_back( false);	    	  
	      }	      	    	  
	   }	
}

//read an array of bits
void readBitArrays( bit_file_c& bf, bool* bits, unsigned cnt )
{
	/* read single bits */
	int value = 0;
	for ( unsigned i = 0; i < cnt; i++ )
	{
	    value = bf.GetBit();
	    if(value == EOF)
	    {
		    cerr << "Error: reading bit" << endl;
		    bf.Close();
		    return;
		}
		if(value == 1)
		{
		   bits[i] = true;		   
		}
		else
		{
		   bits[i] = false;		   
		}
     }	
}

//read DELs
void readDEL(bit_file_c& bf, vector<int>* DEL, int del_count)
{
	for(int i=0;i<del_count;i++)
	{
		unsigned del = readBitVINT( bf );
		DEL->push_back(del);
	}
}

//read INSs
void readINS(bit_file_c& bf, vector<string>* INS, int ins_count)
{
	vector<char>* gens = new vector<char>();  //vector of genLetters
	readBitGens( bf, 2*ins_count, gens);
	vector<char>::iterator It;
	It = gens->begin();
	while(It != gens->end())
	{
		char s[3];
		s[0] = *It;
		*It++;
		s[1] = *It;
		*It++;
		s[2] = '\0';
		//string sc(s);
		INS->push_back(s);
	}
}

//read a vector of gene letters
void readBitGens( bit_file_c& bf, unsigned count, vector<char>* gens )
{				
	int value1 = 0;
	int value2 = 0;
	for( unsigned i = 0; i < count; i++ )
	{
	   value1 = bf.GetBit();
	   value2 = bf.GetBit();
				
	   if( value1 == 0 && value2 == 0 )
	   {
		  gens->push_back('A');
	   }
	   else if( value1 == 0 && value2 == 1 )
	   {
		  gens->push_back('T');
	   }
	   else if( value1 == 1 && value2 == 0 )
	   {
		  gens->push_back('C');
	   }
	   else
	   {
		  gens->push_back('G');
	   }						
	}	
}

//read a genotype.
void readBitGens( bit_file_c& bf,  string & genotype )
{
         //e.g. genotype = "A/G".
        stringstream s;
	int value1 = bf.GetBit();
	int value2 = bf.GetBit();
	if( value1 == 0 && value2 == 0 )
	{
             s<<"A/";
	}
	else if( value1 == 0 && value2 == 1 )
	{
             s<<"T/";
	}
	else if( value1 == 1 && value2 == 0 )
	{
             s<<"C/";
	}
	else
	{
             s<<"G/";
	}						
	value1 = bf.GetBit();
	value2 = bf.GetBit();
	if( value1 == 0 && value2 == 0 )
	{
             s<<"A/";
	}
	else if( value1 == 0 && value2 == 1 )
	{
             s<<"T/";
	}
	else if( value1 == 1 && value2 == 0 )
	{
             s<<"C/";
	}
	else
	{
             s<<"G/";
	}						
        genotype = s.str(); 
}
//read a VINT 
unsigned readBitVINT( bit_file_c& bf )
{
	char b = (char)bf.GetBit();
	for( unsigned j = 1; j < 8; j++ )
	{
		b <<= 1;
		int value = bf.GetBit();
		b |= value;
	}
				
	signed i = b & 0x7F;	
	for( signed shift = 7; (b & 0x80) != 0; shift += 7 ) {
		  b = (char)bf.GetBit();
		  for( unsigned j = 1; j < 8; j++ )
		  {
			  b <<= 1;
			  int value = bf.GetBit();
			  b |= value;
		  }
	      i |= (b & 0x7F) << shift;
	}
	return (unsigned)i;	
}
/*
//read a string
string readString( bit_file_c& bf )
{
	 
	int strSZ = bf.GetChar();	
        cout<<"Size = "<<strSZ<<endl;	
	if( strSZ == EOF )
		return "";		
	char* str = new char[strSZ+1];
	for( int i = 0; i < strSZ; i++ )
	{
		str[i] = (char)bf.GetChar();
	}
	str[strSZ] = '\0';		
        cout<<"Read = "<<str<<endl;
	return string(str);
}
*/

//read a string
string readString( bit_file_c& bf )
{
	 
        stringstream ss;		
	for( ; ; )
	{
            char c = (char)bf.GetChar();
            if(c=='\0')
               break;
            else
	       ss << c;
	}
        //cout<<"Read = "<<ss.str()<<endl;
	return ss.str();
}
